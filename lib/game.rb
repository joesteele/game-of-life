class Game
  attr_accessor :board

  def initialize
    @board = Board.new
  end

  class Board
    attr_accessor :cells

    def initialize
      @cells = []
    end

    def add_cell(cell)
      cell.board = self
      @cells << cell
    end

    def tick
      @cells = surviving_cells + new_cells
    end

    def surviving_cells
      @cells.select { |cell| cell.will_live }
    end

    def new_cells
      cells = []
      seen_coords = []
      @cells.each do |cell|
        (-1..1).each do |x|
          (-1..1).each do |y|
            coords = [cell.x + x, cell.y + y]
            next if seen_coords.include?(coords)
            seen_coords << coords
            new_cell = Game::Cell.new(*coords)
            new_cell.board = self
            if !contains_cell(new_cell) && new_cell.neighbors.length == 3
              cells << new_cell
            end
          end
        end
      end
      cells
    end

    def contains_cell(other_cell)
      @cells.any? { |cell| cell.is_equal_to(other_cell) }
    end
  end

  class Cell
    attr_accessor :x, :y, :board

    def initialize(x=nil, y=nil)
      raise ArgumentError, 'An X and a Y coordinate are required' unless x && y
      @x = x
      @y = y
    end

    def neighbors
      board.cells.select { |cell|
        is_neighbor_to(cell) && !is_equal_to(cell)
      }
    end

    def will_live
      [2,3].include?(neighbors.length)
    end

    def is_equal_to(other_cell)
      x == other_cell.x && y == other_cell.y
    end

    def is_neighbor_to(other_cell)
      x_diff = x - other_cell.x
      y_diff = y - other_cell.y
      (-1..1).include?(x_diff) && (-1..1).include?(y_diff)
    end
  end
end
