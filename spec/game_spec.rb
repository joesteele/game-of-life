require 'spec_helper'

describe Game do
  let(:game) { Game.new }
  let(:board) { game.board }
  let(:cell_a) { Game::Cell.new(1, 0) }
  let(:cell_b) { Game::Cell.new(1, 1) }
  let(:cell_c) { Game::Cell.new(1, 2) }

  describe Game::Board do
    it 'has cells' do
      board.cells.should be_kind_of(Array)
    end

    describe '#add_cell' do
      it 'adds a Game::Cell to the board' do
        board.cells.length.should == 0
        board.add_cell Game::Cell.new(1, 1)
        board.cells.length.should == 1
      end

      it "sets the cell's board to itself" do
        cell = Game::Cell.new(1, 1)
        board.add_cell cell
        cell.board.should == board
      end
    end

    describe '#tick' do
      let(:new_cell) { Game::Cell.new(0, 1) }

      it 'replaces the current gen of cells with the next gen of cells' do
        board.add_cell cell_a
        board.add_cell cell_b
        board.add_cell cell_c
        board.tick

        board.cells.length.should == 3
        board.cells.any? { |cell| cell.is_equal_to(new_cell) }.should == true
        board.cells.any? { |cell| cell.is_equal_to(cell_a) }.should == false
      end
    end

    describe '#surviving_cells' do
      before 'each' do
        board.add_cell cell_a
        board.add_cell cell_b
        board.add_cell cell_c
      end

      it 'returns the cells that will_live' do
        board.surviving_cells.should include(cell_b)
      end

      it 'does not return the cells that will not live' do
        board.surviving_cells.should_not include(cell_a)
        board.surviving_cells.should_not include(cell_c)
      end
    end

    describe '#new_cells' do
      let(:new_cell_a) { Game::Cell.new(0, 1) }
      let(:new_cell_b) { Game::Cell.new(2, 1) }

      before 'each' do
        board.add_cell cell_a
        board.add_cell cell_b
        board.add_cell cell_c
      end

      it 'returns new cells for coords that had 3 neighbors in current gen' do
        board.new_cells.any? { |cell| cell.is_equal_to(new_cell_a) }.should == true
        board.new_cells.any? { |cell| cell.is_equal_to(new_cell_b) }.should == true
      end
    end
  end

  describe Game::Cell do
    it 'requires x/y coordinates on initialization' do
      expect { Game::Cell.new }.to raise_error(ArgumentError, 'An X and a Y coordinate are required')
    end

    describe '#neighbors' do
      before 'each' do
        board.add_cell cell_a
        board.add_cell cell_b
        board.add_cell cell_c
      end

      it 'returns the alive cells around it' do
        cell_b.neighbors.should include(cell_a, cell_c)
      end

      it 'does not return cells not directly adjacent to it' do
        cell_d = Game::Cell.new(4, 5)
        board.add_cell cell_d
        cell_b.neighbors.should_not include(cell_d)
      end

      it 'does not include itself as a neighbor' do
        cell_b.neighbors.should_not include(cell_b)
      end
    end

    describe '#will_live' do
      let(:cell_d) { Game::Cell.new(0, 1) }
      let(:cell_e) { Game::Cell.new(2, 1) }

      it 'returns true with 2 neighbors' do
        board.add_cell cell_a
        board.add_cell cell_b
        board.add_cell cell_c
        cell_b.will_live.should == true
      end

      it 'returns true with 3 neighbors' do
        board.add_cell cell_a
        board.add_cell cell_b
        board.add_cell cell_c
        board.add_cell cell_d
        cell_b.will_live.should == true
      end

      it 'returns false with less than 2 neighbors' do
        board.add_cell cell_a
        board.add_cell cell_b
        cell_b.will_live.should == false
      end

      it 'returns false with more than 3 neighbors' do
        board.add_cell cell_a
        board.add_cell cell_b
        board.add_cell cell_c
        board.add_cell cell_d
        board.add_cell cell_e
        cell_b.will_live.should == false
      end
    end

    describe '#is_equal_to' do
      it 'returns true for cells with the same x/y coords' do
        cell_a = Game::Cell.new(1,1)
        cell_b = Game::Cell.new(1,1)
        cell_a.is_equal_to(cell_b).should == true
      end

      it 'returns false for cells with the same x, but diff y' do
        cell_a = Game::Cell.new(1,1)
        cell_b = Game::Cell.new(1,2)
        cell_a.is_equal_to(cell_b).should == false
      end

      it 'returns false for cells with the same y, but diff x' do
        cell_a = Game::Cell.new(1,1)
        cell_b = Game::Cell.new(2,1)
        cell_a.is_equal_to(cell_b).should == false
      end

      it 'returns false for cells with different coords' do
        cell_a = Game::Cell.new(1,0)
        cell_b = Game::Cell.new(2,1)
        cell_a.is_equal_to(cell_b).should == false
      end
    end

    describe '#is_neighbor_to' do
      it 'returns true for adjacent cells' do
        cell_a = Game::Cell.new(1,1)
        cell_b = Game::Cell.new(1,2)
        cell_a.is_neighbor_to(cell_b).should == true
      end

      it 'returns false for non-adjacent cells' do
        cell_a = Game::Cell.new(1,1)
        cell_b = Game::Cell.new(4,5)
        cell_a.is_neighbor_to(cell_b).should == false
      end
    end
  end
end
